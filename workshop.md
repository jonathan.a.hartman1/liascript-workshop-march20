<!--
@important: <b style="color: blue"> Important! </b>

@fancy_text: <@2 style="color: @1"> @0 </@2>
-->

# Example LiaScript File

This is an example LiaScript File created by {your name here}.

> May the force be with you
>
> -- Jean-Luc Picard (LOTR: The Two Towers)

@fancy_text(`This is some, Fancy Text!`, blue, b)

# Main Content

Here is where we'll talk about the main topic

And here is **some bold text**

And here is some ~strikethrough~

@important This is also important

## *Sub Point A*

There might be several..

## ~Sub Point B~

..sections of things..

## Sub Point C

.. we might want to talk about.

1. With
2. Multiple
3. Points

  - To discuss
  - at length

4. In lists

### Side Note
